const express = require('express') // import express
const app = express()
const fs = require('fs')
const moment = require('moment')
const querystring = require('querystring')
const { Sequelize, Op } = require('sequelize')

const bodyParser = require('body-parser') // for parsing the body of the request
const { Cars } = require('./models')
const { TypeCar } = require('./models')

const multer = require('multer') // for file upload
const path = require('path') // for path
const diskStorage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, path.join(__dirname, './uploads')) // set the destination
  },
  filename: function (req, file, cb) {
    cb(
      null,
      file.fieldname + '-' + Date.now() + path.extname(file.originalname),
    ) // set the file name dynamically
  },
})

const axios = require('axios')
const PORT = process.env.PORT || 8000

app.set('view engine', 'ejs')

app.use(express.static('public'))
app.use(express.json())
app.use(express.static('uploads'))
app.use(bodyParser.urlencoded({ extended: true })) // for parsing application/x-www-form-urlencoded
app.use(bodyParser.json()) // for parsing application/jso

// ==================================== LIST CAR AND SEARCH CAR=============
app.get('/', async (req, res) => {
  try {
    // make temp for search
    let cars = []
    let searchcar = req.query.car
    var empty = ''
    var imageurl = ''
    if (!searchcar) {
      const car = await Cars.findAll()
      const typeCar = await TypeCar.findAll()
      cars.push(car)
      res.render('listcar', {
        title: 'List Car',
        cars,
        typeCar,
        empty,
        imageurl,
      })
    } else if (searchcar != '') {
      const typeCar = await TypeCar.findAll()
      const filterCar = await Cars.findAll({
        where: { name: { [Op.like]: `%${searchcar}%` } },
      })
      // const filterCar = await Cars.findOne({ where: { name: searchcar } })
      if (filterCar == '') {
        empty = 'Mobil tidak ditemukan'
        imageurl = 'img/asset-car.png'
      } else {
        cars.push(filterCar)
      }

      res.render('listcar', {
        title: 'List Car',
        cars,
        typeCar,
        empty,
        imageurl,
      })
    }
    // res.status(200).json(cars)
    // res.status(200).json(typeCar)
  } catch (error) {
    res.status(500).json({ error: error.message })
  }
})
// ========================================================================

app.post('/', async (req, res) => {
  try {
    const cars = await Cars.create(req.body)
    res.status(201).json(cars)
  } catch (error) {
    res.status(500).json({ error: error.message })
  }
})

// ==================================== ADD TYPE CAR =============
app.post('/typecar', async (req, res) => {
  try {
    const typeCar = await TypeCar.create(req.body)
    res.status(201).json(typeCar)
  } catch (error) {
    res.status(500).json({ error: error.message })
  }
})

app.get('/addcar', (req, res) => {
  res.render('addcar', { title: 'Add New Car' })
})

// ==================================== ADD NEW CAR =============
app.post(
  '/api/v1/Cars',
  multer({ storage: diskStorage }).single('images'),
  async (req, res, next) => {
    const car = new Cars({
      name: req.body.name,
      priceperday: req.body.priceperday,
      images: req.file.filename,
      typeId: req.body.typeId,
    })
    console.log(req.body)
    console.log(req.file)
    car.save().then((result) => {
      res.status(201).json(result)
    })
  },
)

app.get('/api/v1/Cars', async (req, res) => {
  try {
    let cars = await Cars.findAll({
      include: [{ model: TypeCar, as: 'typecar' }],
    })
    res.status(200).json(cars)
  } catch (error) {
    res.status(500).json({ error: error.message })
  }
})

// ==================================== UPDATE CAR =============
app.get('/editcar/:id', async (req, res) => {
  try {
    const cars = await Cars.findOne({
      where: { id: req.params.id },
    })
    res.render('editCar', { title: 'Edit Car', cars })
    // res.status(200).json(cars)
  } catch (error) {
    res.status(500).json({ error: error.message })
  }
})

app.put(
  '/updatecar/:id',
  multer({ storage: diskStorage }).single('images'),
  async (req, res) => {
    try {
      const { name, priceperday, typeId } = req.body
      const images = req.file.filename
      const cars = await Cars.findOne({
        where: { id: req.params.id },
      })
      if (cars) {
        if (req.file) {
          fs.unlink(path.join(__dirname, './uploads/' + cars.images), (err) => {
            if (err) {
              console.log(err)
            }
          })
        }
        await Cars.update(
          {
            name: req.body.name,
            priceperday: req.body.priceperday,
            images: req.file.filename,
            typeId: req.body.typeId,
          },
          {
            where: { id: req.params.id },
          },
        )
        res.redirect('/')
      } else {
        res.send('<script>window.location.href="/";</script>')
      }
    } catch (error) {
      res.status(500).json({ error: error.message })
    }
  },
)

// ================================DELETE CARS===============================
app.get('/deletecar/:id', async (req, res) => {
  try {
    Cars.destroy({
      where: {
        id: req.params.id,
      },
    })
    // console.log('delete cars berhasil')

    // res.redirect('/')
    res.send('<script>window.location.href="/";</script>')
    // console.log('delete TypeCar berhasil')
  } catch (error) {
    res.status(500).json({ error: error.message })
  }
})

app.use('/', (req, res) => {
  res.status(404).send('Halaman tidak ditemukan!')
})

app.listen(PORT, () => {
  console.log(`Express nyala di http://localhost:${PORT}`)
})
