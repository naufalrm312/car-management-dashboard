# Challenge 5 Fullstack Web Development

Deskripsi:
Projek membuat CRUD sederhana dengan menggunakan express.js sebagai framework dari node.js.

Anggota Kelompok 2:

1. Bryan Fajar Azhari
   - Add new car
2. Yehezkiel Victorious Ermanto
   - Navbar, Search, dan migration Database
3. Dalih Nurdiansah
   - Edit car
4. Naufal Rizqullah Mubarak
   - List car, filter car by size
5. Nur Zidan Haq
   - Delete car

### Entity Relational Diagram

![ERD](public/img/ERD_car_management_.PNG "ERD")

#

### Run this project

Menggunakan NPM

```bash
npm start
```

Menggunakan Yarn

```bash
yarn start
```
