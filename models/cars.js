'use strict'
const { Model } = require('sequelize')
module.exports = (sequelize, DataTypes) => {
  class Cars extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      Cars.belongsTo(models.TypeCar, { foreignKey: 'typeId' })
    }
  }
  Cars.init(
    {
      name: DataTypes.STRING,
      priceperday: DataTypes.FLOAT,
      images: DataTypes.TEXT,
      typeId: DataTypes.INTEGER,
    },
    {
      sequelize,
      modelName: 'Cars',
    },
  )

  return Cars
}
